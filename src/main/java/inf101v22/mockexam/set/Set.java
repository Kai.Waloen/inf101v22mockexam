package inf101v22.mockexam.set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Set<T> implements ISet<T> {

    ArrayList<T> arrayList = new ArrayList<>();

    @Override
    public int size() {
        return arrayList.size();
    }

    @Override
    public void add(T element) {
        arrayList.add(element);
    }

    @Override
    public void addAll(Iterable<T> other) {
        Iterator<T> otherIterator = other.iterator();
        while (otherIterator.hasNext()) {
            arrayList.add((T) other);
        }
    }

    @Override
    public void remove(T element) {
        for (T elementCheck : arrayList) {
            if (elementCheck == element) {
                arrayList.remove(element);
            }
        }
    }

    @Override
    public boolean contains(T element) {
        for (T elementCheck : arrayList) {
            if (elementCheck == element) {
                return true;
            }
        }
        return false;
    }


    @Override
    public ISet<T> union(ISet<T> other) {
        ArrayList <T> unionArray = new ArrayList<>();
        unionArray.addAll(arrayList);
        unionArray.addAll((Collection<? extends T>) other);

        return (ISet<T>) unionArray;
    }

    @Override
    public ISet<T> intersection(ISet<T> other) {
        ArrayList <T> intersectionArray = new ArrayList<>();
        for (T items : arrayList) {
            if (other.contains(items)) {
                intersectionArray.add(items);
            }
        }

        return (ISet<T>) intersectionArray;
    }

    @Override
    public ISet<T> complement(ISet<T> other) {
        ArrayList <T> complementArray = new ArrayList<>();
        for (T items : arrayList) {
            if (!other.contains(items)) {
                complementArray.add(items);
            }
        }
        for (T items: other) {
            if (!arrayList.contains(items)) {
                complementArray.add(items);
            }
        }
        return (ISet<T>) complementArray;
    }



    @Override
    public ISet<T> copy() {
        ArrayList<T> arrayListCopy = new ArrayList<>();
        arrayListCopy.addAll(arrayList);

        return (ISet<T>) arrayListCopy;
    }


    @Override
    public Iterator<T> iterator() {
        return null;
    }
}

