package inf101v22.mockexam.traffic.model;


import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;
import inf101v22.mockexam.observable.Observer;

import java.util.Timer;

public class TrafficLightModel implements TrafficLightControllable {
    ControlledObservable<Boolean> greenLight;
    ControlledObservable<Boolean> yellowLight;
    ControlledObservable<Boolean> redLight;


    public TrafficLightModel() {
        this.greenLight = new ControlledObservable<>(false);
        this.yellowLight = new ControlledObservable<>(false);
        this.redLight = new ControlledObservable<>(false);

    }

    @Override
    public void goToNextState() {
        int state = 0;
        int counter = state +1;
        switch(counter) {
            case 1:
                this.redLight = new ControlledObservable<>(true);
                break;
            case 2:
                yellowIsOn();
        }
    }

    @Override
    public int minMillisInCurrentState() {
        if (redLight.getValue()) {
            return 2000;}
        if (yellowLight.getValue()) {
            return 1000;}
        if (greenLight.getValue()) {
            return 2000;}
        if (redLight.getValue() && yellowLight.getValue()) {
            return 500;}
        return 0;
    }

    public ControlledObservable<Boolean> getGreenLight() {
        return greenLight;
    }

    public void setGreenLight(ControlledObservable<Boolean> greenLight) {
        this.greenLight = greenLight;
    }

    public ControlledObservable<Boolean> getYellowLight() {
        return yellowLight;
    }

    public void setYellowLight(ControlledObservable<Boolean> yellowLight) {
        this.yellowLight = yellowLight;
    }

    public ControlledObservable<Boolean> getRedLight() {
        return redLight;
    }

    public void setRedLight(ControlledObservable<Boolean> redLight) {
        this.redLight = redLight;
    }

    @Override
    public Observable<Boolean> redIsOn() {
        this.redLight.setValue(true);
        return this.redLight;
    }

    @Override
    public Observable<Boolean> yellowIsOn() {
        this.yellowLight = new ControlledObservable<>(true);
        return this.yellowLight;
    }

    @Override
    public Observable<Boolean> greenIsOn() {
        this.greenLight = new ControlledObservable<>(true);
        return this.greenLight;

    }

}