package inf101v22.mockexam.traffic.view;

import inf101v22.mockexam.observable.Observable;

import javax.swing.*;
import java.awt.*;

public class LampView extends JComponent {
    Observable<Boolean> observable;
    Color color;

    public LampView(Observable<Boolean> observable, Color color) {
        this.observable = observable;
        this.color = color;
        observable.addObserver(this::repaint);

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g.create();

        if (observable.getValue()) {
            g2D.setColor(color);
        } else {
            g2D.setColor(Color.BLACK);
        }

        g2D.fillOval(0, 0, getWidth(), getHeight());
        getPreferredSize();


        repaint();

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(50, 50);
    }

}
