package inf101v22.mockexam.traffic.view;

import inf101v22.mockexam.traffic.model.TrafficLightViewable;

import javax.swing.*;
import java.awt.*;

public class TrafficLightView extends JComponent {

    public TrafficLightView(TrafficLightViewable model) {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        LampView red = new LampView(model.redIsOn(), Color.red);
        LampView yellow = new LampView(model.yellowIsOn(), Color.yellow);
        LampView green = new LampView(model.greenIsOn(), Color.green);
        this.add(red);
        this.add(yellow);
        this.add(green);

    }
}
